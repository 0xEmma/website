import { makeStyles, useTheme } from '@mui/styles'

// import Wallpaper from 'assets/lorikeet.gif'

const useStyles = makeStyles({
  wallpaper: {
    position: 'absolute',
    top: 0,
    left: 0,
    zIndex: -1,
    opacity: 0.3,
    width: '100%',
    height: '100vh',
    backgroundSize: 'cover',
    backgroundPosition: 'center'
    // backgroundImage: `linear-gradient(rgba(255, 255, 255, 0), #06043E), url('${Wallpaper.src}')`
  }
})

interface ParrotBGProps {
  backgroundImage: string
}

const ParrotBG = ({ backgroundImage }: ParrotBGProps) => {
  const classes = useStyles()
  const {
    palette: { mode }
  } = useTheme()

  return mode === 'dark' ? (
    <div
      className={classes.wallpaper}
      style={{
        backgroundImage: `linear-gradient(rgba(255, 255, 255, 0), #06043E), url('${backgroundImage}')`
      }}
    />
  ) : null
}

export default ParrotBG
